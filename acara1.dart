void main() {
  var name = "Hofidatul Afiyah"; // Tipe
  var angka = 22;
  var todayIsMonday = true;
  print(name); // "John"
  print(angka); // 12
  print(todayIsMonday); // false

  // var angka = 100;
  // print(angka == 100); // true
  // print(angka == 20); // false

  var sifat = "rajin";
  print(sifat == "malas"); // true
  print(sifat != "bandel"); //true

  // var angka = 8;
  // print(angka == "8"); // true, padahal "8" adalah string.
  // print(angka == "8"); // false, karena tipe data nya berbeda
  // print(angka == 8); // true

  // var number = 17;
  // print(number < 20); // true
  // print(number > 17); // false
  // print(number >= 17); // true, karena terdapat sama dengan
  // print(number <= 20); // true

  // print(true || true); // true
  // print(true || false); // true
  // print(true || false || false); // true
  // print(false || false); // false

  // print(true && true); // true
  // print(true && false); // false
  // print(false && false); // false
  // print(false && true && true); // false
  // print(true && true && true); // true

  // var sentences = "dart";
  // print(sentences[0]); // "d"
  // print(sentences[2]); // "r"

  // int j = 45;
  // String t = "$j";
  // print("hello" + t);
}
